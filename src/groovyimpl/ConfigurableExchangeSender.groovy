package groovyimpl
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.ConnectionFactory
/**
 * Created by Søren Palmund on 19/11/14.
 */
class ConfigurableExchangeSender {
    public static void main(String[] args) {
        if (args.length == 0) {
            println("Requires queue name as parameter")
            System.exit(-1)
        }

        def factory = new ConnectionFactory(host: 'datdb.cphbusiness.dk', username: 'ivan', password: 'cph')
        def connection = factory.newConnection()
        def channel = connection.createChannel()

        def exchange = "gylle"
        channel.exchangeDeclare(exchange, "topic", false, true, [:])
        println "Connected to '$exchange'!"

        def routingKey = "ost.o"

        println "Routing Key: $routingKey"
        println "Press Ctrl-C to quit"

        def scanner = new Scanner(System.in)
        while (true) {
            def line = scanner.nextLine()

            channel.basicPublish(exchange, routingKey, null as AMQP.BasicProperties, line.bytes)
        }
    }
}