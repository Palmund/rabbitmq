package groovyimpl

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.QueueingConsumer

/**
 * Created by Søren Palmund on 19/11/14.
 */
class ExchangeReceiver {
    public static final String QUEUE_NAME = 'reply'

    public static void main(String[] args) {
        def factory = new ConnectionFactory(host: 'datdb.cphbusiness.dk', username: 'ivan', password: 'cph')
        def connection = factory.newConnection()
        def channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        println("Waiting for messages...")

        def consumer = new QueueingConsumer(channel)
        channel.basicConsume(QUEUE_NAME, true, consumer)

        while (true) {
            def delivery = consumer.nextDelivery()
            def message = new String(delivery.body)
            printf(" [x] Received %s\n", message)
        }
    }
}