package groovyimpl

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.ConnectionFactory
import groovyimpl.model.LoanRequest

class Exchanger {
    public static void main(String[] args) {
        def factory = new ConnectionFactory(host: 'datdb.cphbusiness.dk', username: 'ivan', password: 'cph')
        factory.newConnection().with { connection ->
            connection.createChannel().with { channel ->
                def properties = new AMQP.BasicProperties(replyTo: 'reply')
                def message = new LoanRequest("1605789787", 10.0f, 360).toJson()
                channel.basicPublish("cphbusiness.bankJSON", "", properties, message.bytes)
                println " [x] Send '$message'"
            }
        }
    }
}