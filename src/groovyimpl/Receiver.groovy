package groovyimpl

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.QueueingConsumer

/**
 * Created by palmund on 12/11/14.
 */
class Receiver {

    public static final String QUEUE_NAME = 'send-queue'

    public static void main(String[] args) {
        def factory = new ConnectionFactory(host: 'localhost')
        def connection = factory.newConnection()
        def channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        println("Waiting for messages...")

        def consumer = new QueueingConsumer(channel)
        channel.basicConsume(QUEUE_NAME, true, consumer)

        while (true) {
            def delivery = consumer.nextDelivery()
            def message = new String(delivery.body)
            printf(" [x] Received %s\n", message)
        }
    }
}