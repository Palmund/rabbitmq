package groovyimpl

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.QueueingConsumer

/**
 * Created by Søren Palmund on 19/11/14.
 */
class CatchAllReceiver {
    public static void main(String[] args) {
        def factory = new ConnectionFactory(host: 'datdb.cphbusiness.dk', username: 'ivan', password: 'cph')
        def connection = factory.newConnection()
        def channel = connection.createChannel()

        def exchange = "gylle"
        channel.exchangeDeclare(exchange, "topic", false, true, [:])
        println "Connected to '$exchange'!"

        def queueName = channel.queueDeclare().queue
        println "Queue '$queueName' declared!"

        def routingKey = "ost.*"
        channel.queueBind(queueName, exchange, routingKey)
        println "Routing key ($routingKey) bound to queue '$queueName'"
        println()
        println "Waiting for messages..."

        def consumer = new QueueingConsumer(channel)
        channel.basicConsume(queueName, true, consumer)

        while (true) {
            def delivery = consumer.nextDelivery()
            def message = new String(delivery.body)
            printf(" [x] Received %s\n", message)
        }
    }
}