package groovyimpl

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.ConnectionFactory

/**
 * Created by palmund on 12/11/14.
 */
class Sender {

    public static final String QUEUE_NAME = 'send-queue'

    public static void main(String[] args) {
        def factory = new ConnectionFactory(host: 'localhost')
        def connection = factory.newConnection()
        def channel = connection.createChannel()

        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        def message = "Hello World!"
        channel.basicPublish("", QUEUE_NAME, null as AMQP.BasicProperties, message.bytes)
        printf(" [x] Sent %s\n", message)

        channel.close()
        connection.close()
    }
}
