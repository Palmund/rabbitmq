package groovyimpl.model

import groovy.json.JsonBuilder

public class LoanRequest(var ssn: String, var loanAmount: Float, var loanDuration: Int) {
    fun toJson(): String = JsonBuilder(this).toPrettyString()
}