import com.rabbitmq.client.ConnectionFactory

fun main(args: Array<String>) {
    val factory = ConnectionFactory()
    factory.setHost("localhost")
    val connection = factory.newConnection()
    val channel = connection.createChannel()
    channel.queueDeclare("hello", false, false, false, null)
    val message = "hello world!"
    channel.basicPublish("", "hello", null, message.toByteArray())
    println(" [x] Send '" + message + "'")
}