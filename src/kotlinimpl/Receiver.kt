package kotlinimpl

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.QueueingConsumer

fun main(args: Array<String>) {
    val factory = ConnectionFactory()
    factory.setHost("localhost")
    val connection = factory.newConnection()
    val channel = connection.createChannel()
    channel.queueDeclare("hello", false, false, false, null)
    println("Waiting for messages...")

    val consumer = QueueingConsumer(channel)
    channel.basicConsume("hello", true, consumer)

    while (true) {
        val delivery = consumer.nextDelivery()
        val message = String(delivery.getBody())
        println("Received: '"+message+"'")
    }
}