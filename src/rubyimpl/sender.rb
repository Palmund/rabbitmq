#!/usr/bin/env ruby
# encoding: utf-8

require "bunny"

conn = Bunny.new(:hostname => 'datdb.cphbusiness.dk', :username => 'ivan', :password => 'cph')
conn.start

ch = conn.create_channel
#q  = ch.queue("bunny.examples.hello_world", :auto_delete => true)
q  = ch.queue("*.hello", :auto_delete => true)
x  = ch.default_exchange

run = true
while run
  print '>> '
  msg = gets.chomp
  if msg == 'exit'
    run = false
    next
  end

  x.publish(msg, :routing_key => q.name)
end

sleep 1.0
conn.close