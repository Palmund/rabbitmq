require "bunny"

conn = Bunny.new  :hostname => 'datdb.cphbusiness.dk', 
                  :username => 'ivan',
                  :password => 'cph'
conn.start
ch = conn.create_channel
reply_queue = ch.queue('interestRateReply', :exclusive => true, :auto_delete => true)

reply_queue.subscribe do |delivery_info, metadata, payload|
  puts "Received #{payload}"
end

x = ch.fanout('cphbusiness.bankJSON')

msg = '{"ssn":1605789787,"loanAmount":10.0,"loanDuration":360}'
x.publish msg,  :routing_key => 'cphbusines.bankXML', 
                :reply_to => reply_queue.name

gets.chomp