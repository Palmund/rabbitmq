require "bunny"

conn = Bunny.new(:hostname => 'datdb.cphbusiness.dk', :username => 'ivan', :password => 'cph')
conn.start

ch = conn.create_channel
#q  = ch.queue("bunny.examples.hello_world", :auto_delete => true)
q  = ch.queue("abe.kat", :auto_delete => true)
q1 = ch.queue("*.hello", :auto_delete => true)
x  = ch.default_exchange

q.subscribe do |delivery_info, metadata, payload|
  puts "Received #{payload}"
  x.publish('{"lortelort": 2}', :routing_key => q1.name)
end

puts 'Press any key to exit'
gets.chomp
